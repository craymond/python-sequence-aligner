# PythonSequenceAligner a pybind portage of sequence-aligner

PythonSequenceAligner is a Python3 portage of [sequence-aligner](https://gitlab.inria.fr/craymond/sequence-aligner).  
  
Christian Raymond
[christian.raymond@irisa.fr]

## Example

```python
werEval = SequenceAligner()
alignement = werEval.align_sentence("the quick brown cow jumped over the moon".split(" "), "quick brown cows jumped way over the moon dude".split(" "))
ss = werEval.SummaryStatistics([alignement])
print(ss)
```

Produces the output:

```bash
Align sentences: [100%] |██████████████████████████████████████████████████| 1/1 [ 00:00<00:00 1000.00it/s ] 

# seq   # ref   # hyp   cor     sub     ins     del     WER     SER
1       8       8       0.88    0       0.12    0.12    0.25    1

----------SINGLE ALIGNMENT--------
        # seq   # ref   # hyp   # cor   # sub   # ins   # del   acc     WER     # seq cor
STATS:  1       3       5       3       0       2       0       1       0.67    0
-----   -----   -----   -----   -----   -----   -----   -----   -----   -----   -----
REF:    Pepin   de      Landen  **      *******
HYP:    pepin   de      landen  le      premier

REF=[Pepin      de      Landen                  ]
HYP=[pepin      de      landen  le      premier ]
----------BATCH ALIGNMENT---------
Align sentences: [100%] |██████████████████████████████████████████████████| 2/2 [ 00:00<00:00 2000.00it/s ] 

# seq   # ref   # hyp   cor     sub     ins     del     WER     SER
2       17      8       0       0.47    0       0.53    1       1
```
