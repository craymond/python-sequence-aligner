#include "PythonSequenceAligner.hpp"


PYBIND11_MODULE(PythonSequenceAligner,handle)
{
	handle.doc()="Tools to align sequences of strings using edit distance";
	
	py::class_<PythonSequenceAligner>(handle, "SequenceAligner")
	.def(py::init<const bool,PythonSequenceAligner::cmint>(),py::arg("cs")=false,py::arg("n_jobs")=1)
	.def("align_sentence",&PythonSequenceAligner::pyalign_sentence)
    .def("align_sentences",&PythonSequenceAligner::pyalign_sentences)
	
	;
}