#pragma once
#include "SequenceAligner.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py=pybind11;



class PythonSequenceAligner : public SequenceAligner<std::string>
{

	public:
	PythonSequenceAligner(const bool cs=false,cmint n_jobs=1,cmint substitutionP=DEFAULT_SUBSTITUTION_PENALTY, cmint insertionP=DEFAULT_INSERTION_PENALTY, cmint deletionP=DEFAULT_DELETION_PENALTY) :SequenceAligner(cs,n_jobs,substitutionP, insertionP, deletionP){}

	py::tuple pyalign_sentence(const sequenceSymboles& reference, const sequenceSymboles& hypothesis) const
	{
		auto res=align(reference,hypothesis);
		return py::make_tuple(res.reference,res.hypothesis);
	}
	py::list pyalign_sentences(const std::vector<sequenceSymboles>& reference, const std::vector<sequenceSymboles>& hypothesis) const
	{
		auto res=align(reference,hypothesis);
		py::list l;
		for(const auto& align :res)
			l.append(py::make_tuple(align.reference,align.hypothesis));
		return l;
	}
};
