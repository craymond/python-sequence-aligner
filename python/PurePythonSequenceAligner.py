#!/usr/bin/python3

from enum import IntEnum
import numpy as np


class Penalty(IntEnum):
	DEFAULT_SUBSTITUTION_PENALTY = 100
	DEFAULT_INSERTION_PENALTY = 75 
	DEFAULT_DELETION_PENALTY = 75

	# Values representing string edit operations in the backtrace matrix
class EditOperation(IntEnum):
	OK = 0
	SUB = 1 
	INS = 2
	DEL = 3
	

class SequenceAligner:
	# Result of an alignment.
	# Has a {@link #toString()} method that pretty-prints human-readable metrics.

	class Alignment:
		# @param reference reference words, with null elements representing insertions in the hypothesis sentence
		# @param hypothesis hypothesis words, with null elements representing deletions (missing words) in the hypothesis sentence
		# @param numSubstitutions Number of word substitutions made in the hypothesis with respect to the reference 
		# @param numInsertions Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference
		# @param numDeletions Number of word deletions (necessary words missing) in the hypothesis with respect to the reference		
		def __init__(self, reference, hypothesis,  numSubstitutions,  numInsertions, numDeletions):
			if(reference == None or hypothesis == None or len(reference) != len(hypothesis) or numSubstitutions < 0 or numInsertions < 0 or numDeletions < 0):
				raise Exception('Illegal argument')		
			# Reference words, with null elements representing insertions in the hypothesis sentence and upper-cased words representing an alignment mismatch 
			self.reference=reference			
			# Hypothesis words, with null elements representing deletions (missing words) in the hypothesis sentence and upper-cased words representing an alignment mismatch 
			self.hypothesis=hypothesis			
			# Number of word substitutions made in the hypothesis with respect to the reference 
			self.numSubstitutions=numSubstitutions			
			# Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference 
			self.numInsertions=numInsertions			
			# Number of word deletions (necessary words missing) in the hypothesis with respect to the reference 
			self.numDeletions=numDeletions
	
		
		# Number of word correct words in the aligned hypothesis with respect to the reference.
		# @return number of word correct words 		 
		def getNumCorrect(self):
			return self.getHypothesisLength() - (self.numSubstitutions + self.numInsertions) # Substitutions are mismatched and not correct, insertions are extra words that aren't correct
		
		
		# @return true when the hypothesis exactly matches the reference 
		def isSentenceCorrect(self):
			return self.numSubstitutions == 0 and self.numInsertions == 0 and self.numDeletions == 0		
		
		# Get the length of the original reference sequence.
		# This is not the same as {@link #reference}.length(), because that member variable may have null elements 
		# inserted to mark hypothesis insertions.
		# 
		# @return the length of the original reference sequence		 
		def getReferenceLength(self):
			return len(self.reference) - self.numInsertions
				
		# Get the length of the original hypothesis sequence.
		# This is not the same as {@link #hypothesis}.length(), because that member variable may have null elements 
		# inserted to mark hypothesis deletions.
		# 
		# @return the length of the original hypothesis sequence		 
		def getHypothesisLength(self):
			return len(self.hypothesis) - self.numDeletions
		
		def getAlignedSentences(self):
			hyp= ["*" if h is None else h for h in self.hypothesis]
			ref = ["*" if r is None else r for r in self.reference]			
			return ref,hyp

		def __str__(self):
			ref="REF:\t"
			hyp="HYP:\t"
			for i in range(len(self.reference)): #for(int i=0 i<reference.length i++) {
				if self.reference[i] == None: 
					for j in range(len(self.hypothesis[i])):
						ref+="*"					
				else:
					ref+=self.reference[i]
				
				
				if(self.hypothesis[i] == None):
					for j in range(len(self.reference[i])): 
						hyp+="*"
				else:
					hyp+=self.hypothesis[i]
				

				if(i != len(self.reference) - 1):
					ref+="\t"
					hyp+="\t"
				
			sb = []			
			sb.append("\t# seq\t# ref\t# hyp\t# cor\t# sub\t# ins\t# del\tacc\tWER\t# seq cor\t\n")
			sb.append("STATS:\t")
			sb.append("1\t")
			sb.append(str(self.getReferenceLength())+"\t")
			sb.append(str(self.getHypothesisLength())+"\t")
			sb.append(str(self.getNumCorrect())+"\t")
			sb.append(str(self.numSubstitutions)+"\t")
			sb.append(str(self.numInsertions)+"\t")
			sb.append(str(self.numDeletions)+"\t")
			sb.append(str(round(self.getNumCorrect() / float(self.getReferenceLength()),2))+"\t")
			sb.append(str(round((self.numSubstitutions + self.numInsertions + self.numDeletions) / float(self.getReferenceLength()),2))+"\t")
			if self.isSentenceCorrect():
				sb.append("1")
			else:
				sb.append("0")

			sb.append("\n")
			for i in range (11):
				sb.append("-----\t")
			
			sb.append("\n")
			sb.append(ref)
			sb.append("\n")
			sb.append(hyp)
			
			return ''.join(sb)
		
		# Collects several alignment results.
	# Has a {@link #toString()} method that pretty-prints a human-readable summary metrics for the collection of results.
	 
	class SummaryStatistics:

		# Add a new alignment result
		# @param alignment result to add
		def add(self, alignment):
			self.numCorrect += alignment.getNumCorrect()
			self.numSubstitutions += alignment.numSubstitutions
			self.numInsertions += alignment.numInsertions
			self.numDeletions += alignment.numDeletions
			if alignment.isSentenceCorrect():
				self.numSentenceCorrect +=1
			self.numReferenceWords += alignment.getReferenceLength()
			self.numHypothesisWords += alignment.getHypothesisLength()
			self.numSentences+=1
		# Constructor.
		# @param alignments collection of alignments
		# Number of correct words in the aligned hypothesis with respect to the reference 
		def __init__(self,alignments):
			self.numCorrect=0

			# Number of word substitutions made in the hypothesis with respect to the reference 
			self.numSubstitutions=0
			
			# Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference 
			self.numInsertions=0
			
			# Number of word deletions (necessary words missing) in the hypothesis with respect to the reference 
			self.numDeletions=0
			
			# Number of hypotheses that exactly match the associated reference 
			self.numSentenceCorrect=0

			# Total number of words in the reference sequences 
			self.numReferenceWords=0
			
			# Total number of words in the hypothesis sequences 
			self.numHypothesisWords=0
			
			# Number of sentences 
			self.numSentences=0
	
			for a in alignments:
				self.add(a)


				
		def getNumSentences(self):
			return self.numSentences		

		def getNumReferenceWords(self):
			return self.numReferenceWords
				
		def getNumHypothesisWords(self):
			return self.numHypothesisWords
		
		
		def getCorrectRate(self):
			return round(self.numCorrect / float(self.numReferenceWords),2)
		
		
		def getSubstitutionRate(self):
			return round(self.numSubstitutions / float(self.numReferenceWords),2)
		

		def getDeletionRate(self):
			return round(self.numDeletions / float(self.numReferenceWords),2)
		

		def getInsertionRate(self):
			return round(self.numInsertions / float(self.numReferenceWords),2)
				
		# @return the word error rate of this collection 
		def getWordErrorRate(self):
			return round((self.numSubstitutions + self.numDeletions + self.numInsertions) / float(self.numReferenceWords),2)
		
		
		# @return the sentence error rate of this collection 
		def getSentenceErrorRate(self):
			return round((self.numSentences - self.numSentenceCorrect) / float(self.numSentences),2)

		def __str__(self):
			
			sb = []
			sb.append("# seq\t# ref\t# hyp\tcor\tsub\tins\tdel\tWER\tSER\t\n")
			sb.append(str(self.numSentences)+"\t")
			sb.append(str(self.numReferenceWords)+"\t")
			sb.append(str(self.numHypothesisWords)+"\t")
			sb.append(str(self.getCorrectRate())+"\t")
			sb.append(str(self.getSubstitutionRate())+"\t")
			sb.append(str(self.getInsertionRate())+"\t")
			sb.append(str(self.getDeletionRate())+"\t")
			sb.append(str(self.getWordErrorRate())+"\t")
			sb.append(str(self.getSentenceErrorRate()))
			return ''.join(sb)

	# Constructor. 
	# @param substitutionPenalty substitution penalty for reference-hypothesis string alignment
	# @param insertionPenalty insertion penalty for reference-hypothesis string alignment
	# @param deletionPenalty deletion penalty for reference-hypothesis string alignment
	def __init__(self,case_sensitive=False,substitutionPenalty=Penalty.DEFAULT_SUBSTITUTION_PENALTY,insertionPenalty=Penalty.DEFAULT_INSERTION_PENALTY, deletionPenalty=Penalty.DEFAULT_DELETION_PENALTY):
		#case sensitive
		self.cas_sensitive=case_sensitive
		#Substitution penalty for reference-hypothesis string alignment 
		self.substitutionPenalty = substitutionPenalty
		#Insertion penalty for reference-hypothesis string alignment 
		self.insertionPenalty = insertionPenalty
		#Deletion penalty for reference-hypothesis string alignment
		self.deletionPenalty = deletionPenalty
	
	
	
	# Produce alignment results for several pairs of sentences.
	# @see #align(String[], String[])
	# @param references reference sentences to align with the given hypotheses 
	# @param hypotheses hypothesis sentences to align with the given references
	# @return collection of per-sentence alignment results
	def align_sentences(self,references, hypotheses):
		if(len(references) != len(hypotheses)):
			raise Exception('IllegalArgument')
		
		if(len(references) == 0):
			return []	
		
		alignments = []
		from tqdm import tqdm
		for ref,hyp in tqdm(zip(references,hypotheses),total=len(references)):
			alignments.append(self.align_sentence(ref, hyp))
		
		return alignments
	
	# Produces {@link Alignment} results from the alignment of the hypothesis words to the reference words.
	# Alignment is done via weighted string edit distance according to {@link #substitutionPenalty}, {@link #insertionPenalty}, {@link #deletionPenalty}.
	# 
	# @param reference sequence of words representing the true sentence will be evaluated as lower.
	# @param hypothesis sequence of words representing the hypothesized sentence will be evaluated as lower.
	# @return results of aligning the hypothesis to the reference 
	def align_sentence(self,reference, hypothesis):
		# Next up is our dynamic programming tables that track the string edit distance calculation.
		# The row address corresponds to an index within the sequence of reference words.
		# The column address corresponds to an index within the sequence of hypothesis words.
		# cost[0][0] addresses the beginning of two word sequences, and thus always has a cost of zero.  
		 
		
		# cost[3][2] is the minimum alignment cost when aligning the first two words of the reference to the first word of the hypothesis 

		#int [][] cost = new int[reference.length + 1][hypothesis.length + 1]
		cost=np.zeros((len(reference)+1,len(hypothesis)+1),dtype=int)
		
		# backtrace[3][2] gives information about the string edit operation that produced the minimum cost alignment between the first two words of the reference to the first word of the hypothesis.
		# If a deletion operation is the minimum cost operation, then we say that the best way to get to hyp[1] is by deleting ref[2].
		 
		#int [][] backtrace = new int[reference.length + 1][hypothesis.length + 1]
		backtrace=np.zeros((len(reference)+1,len(hypothesis)+1),dtype=int)
		# Initialization
		cost[0][0] = 0
		backtrace[0][0] = EditOperation.OK
		
		# First column represents the case where we achieve zero hypothesis words by deleting all reference words.
		for i in range(1,len(cost)):# for(int i=1 i<cost.length i++) {
			cost[i][0] = self.deletionPenalty * i
			backtrace[i][0] = EditOperation.DEL 
		
		
		# First row represents the case where we achieve the hypothesis by inserting all hypothesis words into a zero-length reference.
		for j in range(1,len(cost[0])): #for(int j=1 j<cost[0].length j++) {
			cost[0][j] = self.insertionPenalty * j
			backtrace[0][j] = EditOperation.INS 
		

		# For each next column, go down the rows, recording the min cost edit operation (and the cumulative cost). 
		for i in range(1,len(cost)):
			for j in range(1,len(cost[0])):
				def sequal(s1,s2,cs):
					if cs:
						return s1==s2
					return s1.casefold() == s2.casefold()
				#int subOp, cs  # it is a substitution if the words aren't equal, but if they are, no penalty is assigned.
				if(sequal(reference[i-1],hypothesis[j-1],self.cas_sensitive)):				
					subOp = EditOperation.OK
					cs = cost[i-1][j-1]
				else:
					subOp = EditOperation.SUB
					cs = cost[i-1][j-1] + self.substitutionPenalty
				
				ci = cost[i][j-1] + self.insertionPenalty
				cd = cost[i-1][j] + self.deletionPenalty
				
				mincost = min(cs, min(ci, cd))
				if(cs == mincost):
					cost[i][j] = cs
					backtrace[i][j] = subOp
				elif(ci == mincost):
					cost[i][j] = ci
					backtrace[i][j] = EditOperation.INS				
				else:
					cost[i][j] = cd
					backtrace[i][j] = EditOperation.DEL					
				
			
		
		
		# Now that we have the minimal costs, find the lowest cost edit to create the hypothesis sequence
		alignedReference = []
		alignedHypothesis = []
		numSub = 0
		numDel = 0
		numIns = 0
		i = len(cost) - 1
		j = len(cost[0]) - 1
		while(i > 0 or j > 0):
			if backtrace[i][j] == EditOperation.OK:
				alignedReference.insert(0, reference[i-1]) #.lower() pour identifier les mots correctement reconnus
				alignedHypothesis.insert(0,hypothesis[j-1]) 
				i-=1 
				j-=1
			if backtrace[i][j] == EditOperation.SUB: 
				alignedReference.insert(0, reference[i-1])  #.upper() pour identifier les mots incorrectement reconnus
				alignedHypothesis.insert(0,hypothesis[j-1]) 
				i-=1 
				j-=1
				numSub+=1
				
			if backtrace[i][j] == EditOperation.INS: 
				alignedReference.insert(0, None) 
				alignedHypothesis.insert(0,hypothesis[j-1]) 
				j-=1 
				numIns+=1 
				
			if backtrace[i][j] == EditOperation.DEL: 
				alignedReference.insert(0, reference[i-1]) 
				alignedHypothesis.insert(0,None) 
				i-=1
				numDel+=1
		return SequenceAligner.Alignment(alignedReference, alignedHypothesis, numSub, numIns, numDel)										
	


def test_batch(size):
	#import cProfile
	#cp = cProfile.Profile()
	#cp.enable()
	from timeit import default_timer as timer
	start = timer()
	batchEval = SequenceAligner(case_sensitive=False)
	batch1  = [sentence1 for i in range(size)] 
	batch2  = [sentence2 for i in range(size)] 
	alignments = batchEval.align_sentences(batch1,batch2)
	#cp.disable()
	#cp.print_stats()
	end = timer()
	print("Time needed:",round(end - start,2),"s") 
	ss = batchEval.SummaryStatistics(alignments)
	print(ss)

if __name__ == "__main__":
	werEval = SequenceAligner(case_sensitive=False)
	sentence1  = "the quick brown cow jumped over the moon".split()
	sentence2  = "quick brown cows jumped way over the moon dude".split()				
	#align two sequences
	alignement1 = werEval.align_sentence(sentence1,sentence2)
	alignement2 = werEval.align_sentence("Jerome le titi Banner".split(), "Jerome le banner".split())
	#print global statistics about alignments error
	ss = werEval.SummaryStatistics([alignement2])
	print(ss)
	#get the two aligned  sequences caracter "*" is used to fill holes
	ref,hyp = alignement2.getAlignedSentences()
	print("\nREF=",ref)
	print("HYP=",hyp)
	print()
	test_batch(100)

		
		