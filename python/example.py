#!/usr/bin/python3


def align(size):	
	#from PurePythonSequenceAligner import SequenceAligner
	from PythonSequenceAligner import SequenceAligner	
	from timeit import default_timer as timer
	sentence1  = "the quick brown cow jumped over the moon".split(" ")
	sentence2  = "quick brown cows jumped way over the moon dude".split(" ")
	start = timer()
	batchEval = SequenceAligner(False)
	batch1  = [sentence1 for i in range(size)] 
	batch2  = [sentence2 for i in range(size)] 
	alignments = batchEval.align_sentences(batch1,batch2)
	print('\n',alignments[0])
	end = timer()
	print("\nTime needed: {:.2}s".format(end - start)) 

def load(filename):
	with open(filename) as f:
		return [x.split() for x in f.readlines()]

def OMR(ref,pred):
	#from PurePythonSequenceAligner import SequenceAligner
	from PythonSequenceAligner import SequenceAligner	
	ref = load(ref)
	hyp = load(pred)
	sa = SequenceAligner(False) #,n_jobs=2
	alignments = sa.align_sentences(ref,hyp)
	dictlabel=dict()
	refv = []
	hypv = []
	for a in alignments:
		for w in a[0]:			
			if w not in dictlabel:
				dictlabel[w]=len(dictlabel)
			refv.append(dictlabel[w])
		for w in a[1]:
			if w not in dictlabel:
				dictlabel[w]=len(dictlabel)	
			hypv.append(dictlabel[w])
	
	val=dictlabel['']
	del dictlabel['']
	dictlabel['MISS']=val

	labels_name = [x for x in dictlabel]

	from sklearn.metrics import classification_report
	print(classification_report(refv,hypv,target_names=labels_name))

if __name__ == "__main__":
	import sys
	#OMR(sys.argv[1],sys.argv[2])
	align(100)	