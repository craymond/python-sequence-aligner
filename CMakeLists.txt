cmake_minimum_required(VERSION 3.14)
project(sequence-aligner VERSION 0.1.1)

set(CMAKE_BUILD_TYPE Release)

find_package(Python3 COMPONENTS Interpreter Development)
include(FetchContent)
find_package(Git REQUIRED)

FetchContent_Declare(sequencealigner
    GIT_REPOSITORY https://gitlab.inria.fr/craymond/sequence-aligner.git
    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/sequence-aligner
    )
FetchContent_MakeAvailable(sequencealigner)

add_subdirectory(python)


